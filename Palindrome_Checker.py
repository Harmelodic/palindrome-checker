def check_word(word, reverse_word):
    return word == reverse_word


def reverser(word):
    counter = len(word) - 1
    reverse_word = ""
    while 0 <= counter:
        reverse_word += word[counter]
        counter -= 1
    return reverse_word


def check_palindrome():
    input_word = input("Please enter a word: ").lower()

    if input_word.isalpha():
        reverse_word = reverser(input_word)
        if check_word(input_word, reverse_word):
            print("Congratulations. You have entered a palindrome.")
        else:
            print("Unfortunately you haven't entered a palindrome.")
    else:
        print("Sorry that isn't a word. Please re-execute program.")
