import unittest
import Palindrome_Checker


class PalindromeCheckerTest(unittest.TestCase):
    def test_check_word(self):
        self.assertTrue(Palindrome_Checker.check_word("hannah", "hannah"))
        self.assertFalse(Palindrome_Checker.check_word("kevin", "nivek"))

    def test_reverser(self):
        self.assertEqual(Palindrome_Checker.reverser("hannah"), "hannah")
        self.assertEqual(Palindrome_Checker.reverser("kevin"), "nivek")
        self.assertNotEqual(Palindrome_Checker.reverser("palindrome"), "palindrome")

if __name__ == '__main__':
    unittest.main()
