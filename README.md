<img src="https://github.com/Harmelodic/Palindrome_Checker/blob/master/Palindrome_Checker.png" alt="Logo" height="100">

# Palindrome Checker

[![Build Status](https://travis-ci.org/Harmelodic/Palindrome_Checker.svg?branch=master)](https://travis-ci.org/Harmelodic/Palindrome_Checker)

Python script that checks if a word is a palindrome

##### Runs in: Python Runtime Environment
